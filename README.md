### Markdown Quick Reference &amp; the Basics

This repository contains a single-page *Markdown* document (`Markdown_Notes.md`) created to get up to speed with *Markdown* syntax and tools for writing and publishing. It was finalised with the  *[Code-OSS](https://github.com/microsoft/vscode)* editor. Examples of *Markdown* syntax features within the document served as a quick reference.

The [*HTML*](https://getiton.gitlab.io/md-quick-ref) version was made available for viewing on a browser, converted using **Pandoc** with its *Markdown* variant.
The full command is shown below.

For those who would like to try MD-to-HTML conversion for themselves, read further.

##### Tools required

+ A computer system with a terminal or shell window to execute commands.
  *Manjaro Linux* was used but any system will do.
+ **Git** program
+ **Pandoc** program with *Haskell* libraries installed.

##### Steps to prepare a copy of this repository on your system

```sh
  cd <folder-where-md-quick-ref-will-be>
  git clone https://gitlab.com/getiton/md-quick-ref
  cd md-quick-ref
```

##### The command doing the magic

To convert `Markdown_Notes.md` into _HTML_ format, type in a terminal/shell in one single line.

```sh
  pandoc Markdown_Notes.md
          --to=html
          --output=Markdown_Notes.html
          --metadata title='The Markdown Quick Reference'
          --standalone
          --toc
          --highlight-style=kate
          --css=css/main.css
          --css=css/table.css
          --css=css/code_block.css
          --format=markdown
```

##### For writing in Markdown (Optional)

+ **Code-OSS**, the open-source editor
+ *Code-OSS* extensions: `Markdownlint`, `Terminal`, `Markdown Preview Enhanced`
+ In *Code-OSS* extensions view, set this for **Markdown-preview-enhanced**: *Pandoc Markdown Flavor*,

      markdown
