| | | |
|:---|:-:| :-------------- |
| Last updated|: |2020 Jun 18<sup>th</sup> |
| License     |: |[CC BY-NC-SA version 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/) |
| Source      |:|[GitLab repository](https://gitlab.com/getiton/md-quick-ref/)

<!--
  2020Jun16

  This document can be found online in https://gitlab.com/getiton/md-quick-ref/
  It was finalised on `Code-OSS` (https://github.com/microsoft/vscode) editor.

  The HTML version was made available using `Pandoc` with its `Markdown` variant.
  See the full command under #converting-md-to-html-format' section below.

  Examples of Markdown syntax features throughout this document can be used as a quick reference.

  This section uses HTML comment style to retain content in the generated HTML document while hidden from HTML viewers.

  To keep comments hidden completely from the generated HTML document, a little cheat using *MD* link variation can be used instead. See the original code (in Markdown format) of this HTML document for examples.
-->

[//a0]: # "This comment style is required for hiding content in the corresponding exported __HTML__ document."

---

<br>

# Markdown &mdash; The Basics

### Get Acquainted with Markdown &hellip; Quickly

<!--

Fail __MD__ comments:

[//]:This is not how to write a comment.

[//]: This is not how to write a comment.

[//]: This is not how to write a comment :

[Comment]: : This is not how to write a one-line comment.

[//]: # (This way
does not make this
a multi-line
comment.)

[//]: # "Nested comment in __HTML__ comment works for Ghostwriter but not Pandoc."
-->
[//b0]: # "2020May11"
[//b1]: # "__MD__ link as comment examples."
[//b2]: <https://w.com/#> "<--- This URL is hidden too."

[//b3]: # (This is the most-accepted one-line comment hidden everywhere including GitHub.)
[//b4]: # (This way makes this a multi-line comment.)

[//b5]: # "This is a one-line comment hidden everywhere including react-markdown."
[//b6]: # "This way also makes this a multi-line comment."

[This can only be a one-line comment]::
[This can only be a one-line comment]: :
[This can  ]::
[be a nice ]:: "hidden in HTML"
[multi-line]:: "hidden in HTML"
[comment   ]:: (hidden)

<br>

## Why Markdown?

__Markdown__ (__`MD`__ for short) is a set of mark-up syntax to inject some, but not limited to, simple text manipulation features like headings, lists, paragraphs and emphasis (**bold**, *italics* and ~~strike-through~~) in an otherwise plain-text document. It aims to help writers focus more on content writing by keeping styling separate as much as possible while retaining some controls over the document's structure.

If you are new to __`MD`__, here are a couple of beginner's sites to start you off on the new writing journey. Use them as a quick reference or as a brief introduction to __`MD`__. While this article is geared towards creating static web pages (in __HTML__ formats) using the __Pandoc__ converter, the references focus on the __GitHub-Flavored Markdown__ (__`GFM`__), and the resources and tools related to __`MD`__.

### References

[Markdown Basic Syntax](https://www.markdownguide.org/basic-syntax){.refo}<br>
[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet){.refo}

<br>

---

__2020May15__

## Markdown Standards ... the Lacking of

### References

[Extended Markdown Syntax](https://www.markdownguide.org/extended-syntax){.refo}<br>
[Markdown on Wikipedia](https://en.wikipedia.org/wiki/Markdown){.refo}<br>
[R-Markdown Preface](https://bookdown.org/yihui/rmarkdown/){.refo}

The father of __MD__ standards is the one offered by the creator of __`MD`__, _John Gruber_. However, it is, considered by many, to be severely limited in the variety of features required to aid in the creation of modern technical rich-text digital documents. Salvation for the more adventurous writers came, thankfully, in the form of ***syntax extensions***, such as maths expression, table, auto-linking, footnote (or citation) and syntax-highlighting, etc.

Some of these more useful _syntax extensions_ have been conveniently packaged into variants or flavours. Standards like __GitHub-Flavored Markdown__ (__`GFM`__), __CommonMark__, __MultiMarkdown__ are few of the many variants created. More standards were hatched for the purpose of writing eMails and comments on websites ranging from _StockExchange_ to _OpenStreetMap_.

Meanwhile, to add to the confusions, more and more __MD__ editors and converters are exporting __MD__ into __HTML__ documents using their own variants of the __MD__ standard, if there is one. Although these created a great fog of uncertainty over which __MD__ variants will out-shine its competitors, some of these __MD__ variants are destined to become popular standards due to their simplicity in enriching documents or static web pages.

Consequently, at the beginning of a serious __MD__ document project, in addition to the task of learning the __MD__  syntax, one has to choose

+ an integrated **code** editor (because a simple text editor may not be packed with enough features to deliver a smoother writing experience any more);
+ a suitable __MD__ variant to support most, if not all, of the extensions needed; and
+ a viewing platform for each of the output formats.

<br>

---

__2020Mar12__

## Comments in Markdown

### Definition

While __HTML__ comments used in a __MD__ document are reproduced in the exported __HTML__ document, **true** __MD__ comments are both hidden from view as well as absence in the corresponding __HTML__ document.

### References

[Comments in MD (SO Answers)](https://stackoverflow.com/questions/4823468/comments-in-markdown){.refo}<br>
[MD Comments not in generated output](https://alvinalexander.com/technology/markdown-comments-syntax-not-in-generated-output){.refo}

[test-link]: <http://test.link> "Another http comment sample"

<br>

First thing first, every document needs a comment. Comments in __MD__ can be written in one of these two ways:

a) as an __HTML__ comment, or

b) as a __MD__ comment.

<br>

#### a) As an __HTML__ comment

  ```html
  <!-- Comments here can
       be written in multiple lines
       as in a standard HTML5 document.
  -->
  ```

This HTML-style comment block is hidden from __HTML__ viewers, when the __MD__ document containing it is converted, for example by __Pandoc__, or exported by __Ghostwriter__, into an __HTML__ document. However, a copy of this comment is actually stored in the generated __HTML__ document. Likewise, viewing the same __MD__ document via a web browser extension, as a temporary __HTML__ document, also reproduces a copy of the comment in the temporary generated __HTML__ source. The last option relies on the extension's ability to convert __MD__ into __HTML__ format on-the-fly.

#### b) As a true __MD__ comment

This type of comment is only visible in the __MD__ document and will not be reproduced in the __HTML__ document. It can be achieved by using a little variation of the link definition syntax, ``[link id]: link URL (link title)``, adapted for comments like so,

  ```md
  <blank line>
  [//]: # ( .. comments with parenthesis .. )
  [//]: # " .. or with double-quotes .. "
  [//]: # " .. and as multi-lines .. "
  ```

This is the most widely accepted and compatible style recommended in the reference.

<br/>

---

__2019Nov19__

## Code Syntax-Highlighting

### References

[Syntax Highlight](assemble.io-MarkdownCheatsheet-2018Oct.md#syntax-highlighting){.refo}<br>
[Pandoc Markdown Variants](https://pandoc.org/MANUAL.html#markdown-variants){.refo}

One of the great features first offered by the __`GFM`__ is the use of syntax highlighting within a code-block `` ``` `` mark-up for embedding code. We simply add the desired language file extension after it. For example, we could "fence" our code with `` ```js `` for _JavaScript_ code as follows:

```plain
    ```js
      // this is comment in JavaScript
      function name(arg1, arg2)
      {
        doNothing();
        return 0;
      }
  \```
```

which renders as

```js
    // this is comment in JavaScript
    function name(arg1, arg2)
    {
        doNothing();
        return 0;
    }
```

<br>

**Note** : The fenced marks ` ``` ` should **not be indented**. Otherwise, the entire section may be misinterpreted as a plain-text block.

Although syntax-highlighting feature is now offered by __GFM__, **MultiMarkdown**, **CommonMark** and __Pandoc__'s default __MD__ variant, it is still not fully cross-compatible among the variants. Most variants either support syntax-highlighting partially or not at all. Currently, __Pandoc__ with `--highlight-style=kate` understands these shell code highlighting:

<br>

\`\`\`bash

```bash
  $pandoc -s -highlight-style=kate <in-file>.md -o <out-file>.html
```

<br>

\`\`\`zsh

```zsh
  $pandoc -s -highlight-style=kate <in-file>.md -o <out-file>.html
```

<br>

\`\`\`sh

```sh
  $pandoc -s -highlight-style=kate <in-file>.md -o <out-file>.html
```

<br>

But not these

<br>

\`\`\`console

```console
  $pandoc -s -highlight-style=kate <in-file>.md -o <out-file>.html
```

<br>

\`\`\`shell

```shell
  $pandoc -s -highlight-style=kate <in-file>.md -o <out-file>.html
```

<br>

\`\`\`shell-script

```shell-script
  $pandoc -s -highlight-style=kate <in-file>.md -o <out-file>.html
```

<br>

\`\`\`script

```script
  $pandoc -s -highlight-style=kate <in-file>.md -o <out-file>.html
```

<br>

---

__2020May13__

## Embedding Images or SVG Graphics

### References

[Changing Image Size in MD (SO Answers)](https://stackoverflow.com/questions/14675913/changing-image-size-in-markdown){.refo}<br>
[Styling Images with MD](https://www.xaprb.com/blog/how-to-style-images-with-markdown/){.refo}

The standard __MD__ style to embedded an image is

```md
  ![alt text](/src/of/image.jpg "title")
```

where `alt text` and `title` are optional.

![Sample Image](./Building-LightHouseInHighWaves_260x390.jpg "Lighthouse in high waves")

  <img src="./Building-LightHouseInHighWaves_260x390.jpg" alt="Sample image" title="Lighthouse in high waves" width="240px" height="380px"  style="float: right; margin-left: 40px;"/>

<br>

### No-Fuss Image Embedding

Sadly, the standard __MD__ method for embedding image lacks size and style control. Fortunately, the workaround turns out to be a rather effortless one. Image size control and all the familiar CSS syntax can be made available simply by using the standard __HTML__ code within the __MD__ document. This is by-far the most fail-proof method known to-date.

```html
  <img src="//src/of/image.jpg"
    alt="alt text" title="title"
    width="240px" height="380px"
    style="float: right; margin-left: 40px;"/>

```

<br/>

---

__2020May16__

## Markdown Editors Round-up

### References

[Markdown Editors for Linux](https://www.slant.co/topics/2134/~markdown-editors-for-linux){.refo}<br>
[AlternativeTo Markdown](https://alternativeto.net/software/markdown/){.refo}<br>
[Markdown Editors Shoot-out](https://foliovision.com/2017/02/markdown-tables){.refo}<br>
[Markdown Extended Syntax](https://www.markdownguide.org/extended-syntax){.refo}

Traditionally, the process of __MD__ document creation involves, first, using an editor to prepare the content in a text file, coupled with __MD__ syntax for the basic structure. The __MD__ formatted text file is then exported into __HTML__, or other desired formats, using a separate converter program.

For some writers who may prefer a more streamlined approach, there is also the "one-stop-do-them-all" method, whereby the __MD__ editing features and the multi-format conversion are both rolled into one integrated process. Moreover, additional integrated tools like spell-checker and live-preview allow the writer to stay focus and enjoy the writing experience while the turn-around time for vetting and correction is further reduced.

There are many free-to-use programs for tasks like editing, viewing and multi-format conversion with __MD__ documents. ***Re-Text*** and ***Remarkable*** used to be quite good but have since not been very actively maintained. Also, their performance were found to be buggy and unpredictable at times. Better ones like ***Ghostwriter*** seemed to be an all-rounder and is found to be able to meet the demanding tasks quite comfortably. On the other hand, ***Joplin*** and ***Zettlr*** are over-the-top editors, better suited for managing large book projects.

However, the search for the ideal integrated __MD__ editor can be liken to an on-going holy-grail hunting expedition. Many editors are either too clumsy to use, not specialised for __MD__ or do not have enough support for __MD__ features. That said, we are still spoiled with plenty of choices for integrated __MD__ editors, even when our choices are narrowed down to free-to-use open-source editors for Linux.

__Ghostwriter__ stands-out as the author's best choice mainly because it was born and bred as a pure _MD_ editor. Its workflow strums along extremely well with the writing habits of the author. Honestly, its simplistic yet functional user-interface is best and it is hard to find anything wrong with it as a first-time writer's tool. It has also been made available on all major platforms.

<dl>
  <dt>Ghostwriter</dt>
  <dd>&check; Easy to be familiarised with the editor</dd>
  <dd>&check; Side-by-side editing and preview</dd>
  <dd>&check; Option to export into various formats and with __MD__ variant of choice</dd>
  <dd>&check; One-click export</dd>
  <dd>&check; Opens respective viewer on export</dd>
  <dd>&check; Modern application feel</dd>
  <dd>&check; Built-in multi-language spell-check</dd>
  <dd>&check; Inline __MD__ supports within HTML tags</dd>
  <dd>&cross; Not a great text editor</dd>
</dl>

<br>

If you like to get an idea of how a good integrated workflow can really improve the creating experience and vice versa, please feel free to try the other editors mentioned below.

Next are two notebook editors that are a little over-whelming and an over-kill for a single-page document. But when the time calls for a serious huge book project, these may be a life-savour.

<dl>
  <dt>Joplin</dt>
  <dd>&check; Exports to multiple formats</dd>
  <dd>&check; Encryption</dd>
  <dd>&check; Online & offline storage</dd>
  <dd>&check; Complex project-base document creator</dd>
  <dd>&cross; Fixed UI theme</dd>
  <dd>&cross; Complex features can be a distraction</dd>
  <dd>&cross; Non-adjustable application font-size</dd>

  <dt>Zettlr</dt>
  <dd>&check; Complex project-base document editor</dd>
  <dd>&check; one-click export to multiple formats</dd>
  <dd>&cross; Fixed UI theme</dd>
  <dd>&cross; Non-adjustable application font-size.</dd>
  <dd>&cross; No side-by-side preview</dd>
</dl>

<br>

From the other end of the scale, we have very powerful code editors that can be transformed with plugins into integrated __MD__ editors. A minor downside is that there may be one or more non-standard __MD__ editing features:

<dl>
  <dt>Code-OSS (from VisualStudio-Code)</dt>
  <dd>&check; Great code/text editor - handles multiple encodings, large file size, long single line, etc.
  <dd>&check; Awesome documentation and community support</dd>
  <dd>&check; Many extensions for productivity</dd>
  <dd>&check; Synchronised side-by-side editing and preview</dd>
  <dd>&check; Requires minimal configuration for simple __MD__ editing</dd>
  <dd>&cross; Requires manual configuration to achieve fully integrated advanced __MD__ editing. [more](https://thisdavej.com/build-an-amazing-markdown-editor-using-visual-studio-code-and-pandoc/) </dd>
  <dd>&cross; Requires addition software: __Electron__</dd>

  <dt>SublimeText3</dt>
  <dd>&check; Great community support</dd>
  <dd>&check; Great code/text editor</dd>
  <dd>&check; Many extensions for productivity</dd>
  <dd>&check; Side-by-side preview.</dd>
  <dd>&cross; Non-free, one-time payment</dd>
  <dd>&cross; Non-open-source</dd>
  <dd>&cross; Requires manual configuration to turn it into an integrated __MD__ editor</dd>
  <dd>&cross; __MD__ extensions and syntax-highlights are un-correlated and can be confusing to use</dd>

  <dt>Atom</dt>
  <dd>&cross; slow to start, high memory usage, issue loading file with large size or long lines</dd>
  <dd>&cross; Requires addition software: __Electron__ </dd>
</dl>

<br>

These fell-off the author's list:
<dl>
  <dt>ReText</dt>
  <dd>&check; Synchronised side-by-side preview</dd>
  <dd>&cross; Edit resets on external edits</dd>
  <dd>&cross; Poor __MD__ extensions support</dd>
  <dd>&cross; No __MD__ support within HTML tags</dd>
  <dd>&cross; Not a great code editor</dd>
  <dd>&cross; Poor application theme</dd>

  <dt>Caret</dt>
  <dd>&cross; Non-free, one-time &euro;25</dd>
  <dt>Marker</dt>
  <dd>&cross; Arch-linux AUR package build fail</dd>
  <dt>Haroopad</dt>
  <dd>&cross; No update, non-open-source</dd>
  <dd>&cross; Non-standard __MD__ syntax</dd>
  <dt>Typora</dt>
  <dd>&cross; Graphical table is nice but unable to interpret _pipe_table_</dd>
  <dd>&cross; Free only for beta</dd>
  <dd>&cross; HTML comments does not support multi-line well</dd>
  <dt>Uberwriter</dt>
  <dd>&cross; No update</dd>
  <dt>Remarkable</dt>
  <dd>&cross; No update</dd>
</dl>

<br>

---

## Features Supported by Markdown Variants

|MD Variants |Pandoc MD |GFM |Common Mark |Multi MD |PHP MD Extra |GitLab MD |MD Strict |MD |
|:----- |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|**Features**             | | | | | | | |**extensions**
|`# ATX Heading`          |Y|Y|Y|Y |Y|Y|Y|atx_headings
|`SeText Heading`<br>`==============` |Y|Y|Y|Y |Y|Y|Y|&emsp;
|`<sup>` (3<sup>rd</sup>) |Y|Y|Y|Y |Y|Y|Y|superscript
|`<sub>` (H<sub>2</sub>O) |Y|Y|Y|Y |Y|Y|Y|subscript
|`<center>`               |Y|Y|Y|Y |Y|Y|Y|&emsp;
|Centred Headings<sup>[+](#centred-headings){#local-footnote-a}</sup> |N|Y|Y|N |N|Y|N|&emsp;
|Inline-code Emphasis<sup>[\@](#inline-code-emphasis){#local-footnote-b}</sup> |Y|Y|Y|N |Y|Y|Y|&emsp;
|Table (piped)            |Y|Y|N|Y |Y|Y|N|pipe_tables
|Syntax Highlight[^1]     |Y|Y|Y|Y |N|Y|N|backtick\_code_blocks
|HTML Comment             |Y|Y|Y|Y |Y|Y|Y|&emsp;
|`[]: # (comment)`        |Y|Y|Y|Y |Y|N|Y|&emsp;
|Footnote[^2]             |Y|N|N|Y |Y|N|N|footnotes
|MD Variants |Pandoc MD |GFM |Common Mark |Multi MD |PHP MD Extra |GitLab MD |MD Strict |&emsp;

: Table 1-1 : Features supported in various MD variants.

&plus; Centred Headings[:](#centred-headings){#centred-headings}

```md
#### <center>Centred ATX Heading</center>

<center>Centred SeText Heading</center>
----
```

[↩︎ Back to item](#local-footnote-a)

@ Inline-code Emphasis[:](#inline-code-emphasis){#inline-code-emphasis}

__`bold`__, _`italic`_ or ***`bold and italic code`***

[↩︎ Back to item](#local-footnote-b)

[^1]: Support for syntax of all code/script languages is not always complete. Eg. ` ```bash` may be ok but may not for ` ```shell` or ` ```sh`.

[^2]: While __Pandoc__'s conversion using __`GFM`__ variant does not support footnotes, __Ghostwriter__'s __`GFM`__ export is able to mitigate this issue.

<br/>

---

__2020May26__

## Converting __MD__ to __HTML__ Format

### Reference

[Official Pandoc Manual](https://pandoc.org/MANUAL.html){.refo}

This section addresses the task of converting __MD__ file into __HTML__ web-page to be viewed in a modern browser. However, __MD__ syntax standard has not been very well known for standardization. Converting __MD__ into __HTML__ often pose some ill-interpreted formatting issues.

__Pandoc__ was found to be the most consistent, flexible and straight-forward conversion. It stands out because it offers a selection of the more popular __MD__ variants to be used for conversion, plus addition of any number of __syntax extensions__.

For example, this page was converted using __Pandoc__ with the following __MD__ variant and syntax extensions in order to showcase a wide variety of __HTML__ features listed in [Table 1-1](#features-supported-by-markdown-variants) above.

```sh
  $ pandoc Markdown_Notes.md
      -o Markdown_Notes.html
      --metadata title="The Markdown Quick Reference"
      --css=css/main.css
      --css=css/table.css
      --css=css/block_code.css
      --standalone
      --highlight-style=kate
      --toc
      --format markdown_strict
         +footnotes
         +strikeout
         +pipe_tables+table_captions
         +backtick_code_blocks
         +angle_brackets_escapable
         +markdown_in_html_blocks
         +intraword_underscores
         +header_attributes
         +link_attributes
         +fenced_code_attributes
         +auto_identifiers

```

Equivalently, the __Pandoc__'s default variant includes all these syntax extensions.

```sh
  $ pandoc Markdown_Notes.md
      -o Markdown_Notes.html
      --metadata title="The Markdown Quick Reference"
      --css=css/main.css
      --css=css/table.css
      --css=css/block_code.css
      --standalone
      --highlight-style=kate
      --toc
      --format markdown
```

In general, the __Pandoc__ command format looks like

```sh
  $ pandoc <in-file>.md
      --output <out-file>.html
      --format <MD_variant>[+<extensions>]
      --standalone
      --highlight-style <HL-style>
      --toc
      --css food.css
      --css foot.css
      --include-in-header bar.js
      --include-in-header barn.js ..

  $ pandoc <in-file>.md
      -o <out-file>.html
      -f <MD_variant>[+<extensions>]
      -s
      --toc
      --highlight-style <HL-style>
      --css food.css
      --css foot.css ..
      --include-in-header bar.js
      --include-in-header barn.js ..

```

where `<MD_variant>` is one of the accepted __MD__ variants:

    markdown (default) |
      gfm | markdown_mmd | markdown_phpextra |
      markdown_strict | commonmark

`markdown_strict` follows John Gruber's __MD__ standard, while `markdown` is __Pandoc__'s default standard. __TIP__:  The author found that using `markdown_strict` as a starting-point and add the required extensions, tailored to each specific __HTML__ feature, makes it easier to understand the effects of the individual extension.

The `<extensions>` is any number of these:

    footnotes | table_captions | pipe_tables |
    backtick_code_blocks
      (for syntax-highlighting using Haskell library) |
    header_attributes | link_attributes
      (to add HTML attributes to headers and links) |
    fenced_code_attributes
      (to add HTML attributes to backtick_code_blocks)
    markdown_in_html_blocks |
    intraword_underscores |
    auto_identifiers |
    angle_brackets_escapable |

    ... and many more ...

Not all extensions are listed herein. Refer to [__Pandoc__ syntax extensions](https://pandoc.org/MANUAL.html#extensions) for the complete set.

And `<HL-styles>` is one of these __Pandoc__ supplied styles:

    pygments | tango | espresso | zenburn | kate | monochrome | breezedark | haddock

***Note*** Syntax-highlighting in __Pandoc__ requires _Haskell Skylighting_ library which is about 180 MiB when installed on *Arch-Linux*.

Refer to [*Pandoc* Reference](https://pandoc.org/MANUAL.html#markdown-variants) for details on using __MD__ variants.

<br/>

### A word on Pandoc

Pandoc is a wonderful mark-up-to-multi-format converter, including *MD*-to-*HTML* conversion. However. be forewarned that it is dependent on  external libraries, like Haskell, and they can take up-to nearly 350 MiB when installed. For a smaller installation size, look for a pre-compiled (binary) version of __Pandoc__ specifically for the computer doing the conversion.  For *Arch-Linux*, there is an *AUR* package called `pandoc-bin` which weighs in at a very reasonable size of under 60 MiB when installed. The downside is that it does not have any syntax-highlighting support.

<br>

---

__2020May13__

## Viewing Markdown Directly in Your Browser

For a quick review of your __MD__ file, apart from using the preview function of an integrated __MD__ editor, one can also use a suitable __MD__ viewer browser extension, without actually convert it to __HTML__ format.

On the ***Vivaldi*** browser, an extension, called __Markdown Preview Plus__ provides a reasonable viewing capability. Another popular _Chrome_ extension, __Markdown Viewer__, can also do the job with a bit more finger-jogging installation. First, the extension has to be cloned, or downloaded and unpacked, onto your PC from its repository, [Markdown Viewer on GitHub](https://github.com/simov/markdown-viewer).  Then it has to be loaded into _Vivaldi_ under __developer's mode__ in the extension section. Another great browser extension, __Markdown Viewer Webext__ can be used in _Firefox_ without these tedious steps. Take note that most browser extensions for viewing __MD__ files come with limited variants (called compilers) and lack the support of many rich and useful features (`link_attributes` for example) provided by __Pandoc__. __GitHub__, __GitLab__ and __BitBucket__ are supported due to their popularity.

Also, in cases whereby the browser extension does not support direct viewing of __MD__ files from the local disk, a local web server may be required to serve your __MD__ files via, for example, __`http://localhost:8888`__.  See discussions from [Markdown Viewer on GitHub](https://github.com/simov/markdown-viewer) for additional set-up for different browsers.  On a Linux machine, further set-up may also be required for the operating system to recognise the __MD__ format and activate the viewer as mentioned in [GitHub repository for __Markdown Viewer WebExt__](https://github.com/KeithLRobertson/markdown-viewer#installing-on-linux).

<br/>

---

__2020May26__

## Non-Standard Footnote with Symbol

This inspiration came from the lack of footnote support by __GFM__. Although __Pandoc__ supports footnote, it is restricted to ordered-numbering and the placement of the footnotes is fixed at the end of the document. As numbering footnotes may not be always ideal, it would be nice to be able to use symbols as links to our footnotes.

These limitations can be overcome by one easy cheat with the use of link references together with __Pandoc__ conversion using __`link_attributes`__ syntax extension. For example, we could write a footnote link this way,

```md
This statement has a footnote.
  <sup>[&hearts; tango](#tango-by-symbol){#tango}</sup>
```

which renders as

This statement has a footnote.
  <sup>[&hearts; tango](#tango-by-symbol){#tango}</sup>

and in the document where we want the footnote to be, we could insert a section like so,

```md
<div id="tango-by-symbol">
<span id="tango-footnote">&hearts; tango</span>: This is the footnote for tango.<br>
This note can also be multi-lined with a<br>
convenient link back to the symbol!<br>
[↩︎ Back to passage](#tango)
</div>
```

which gives us a functional and nice custom footnote,

<div id="tango-by-symbol">
<span id="tango-footnote">&hearts; tango</span>: This is the footnote for tango.<br>
This note can also be multi-lined with a<br>
convenient link back to the symbol!<br>
[↩︎ Back to passage](#tango)
</div>

<br>
<br>
<br>
<hr>
<center> ~ EoF ~ </center>
